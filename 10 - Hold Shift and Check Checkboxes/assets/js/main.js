const checkboxes = [...document.querySelectorAll('.inbox input[type="checkbox"]')];
let lastSelectedCheckbox;

function handleCheck(e) {
  let inRange = false;

  if(e.shiftKey){
    const rangeStartIndex = checkboxes.indexOf(lastSelectedCheckbox);
    const rangeEndIndex = checkboxes.indexOf(this);
    let calculatedRangeStartIndex = rangeStartIndex;
    let calculatedRangeEndIndex = rangeEndIndex;

    if(rangeStartIndex > rangeEndIndex){
      calculatedRangeStartIndex = rangeEndIndex;
      calculatedRangeEndIndex = rangeStartIndex;
    }

    const matchedRange = checkboxes.filter((checkbox, key) => {
      return key >= calculatedRangeStartIndex && key <= calculatedRangeEndIndex;
    }).map(
        checkbox => checkbox.checked = lastSelectedCheckbox.checked
    )
  }

  lastSelectedCheckbox = this;

}

checkboxes.forEach(checkbox => checkbox.addEventListener('click', handleCheck));